// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
// CSS RESET
import '@/assets/css/reset.css'
// AXIOS
import axios from 'axios'
Vue.prototype.$axios = axios;
// GLOBAL
import GLOBAL from '@/Global'
Vue.prototype.$global = GLOBAL
// 
Vue.config.productionTip = false



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

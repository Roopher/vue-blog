// global import
import Vue from 'vue'

// global variate
const globalUrlHttp = 'https://webapp-demo2.firebaseio.com/blogaddposts/'
const globalUrl = 'https://webapp-demo2.firebaseio.com/blogaddposts.json'
let abc = (num) => {
    if(!num){
        return 20
    }else{
        return 20 + num
    }
}
const initObj = {
    title:'全局自定义变量',
    content:'常量、变量、函数方法',
    type:'String、Number、Array、Object'
}

// 自定义指令

// 管道符
Vue.filter('filterText',function(data){
    return data.slice(0,100) + '...'
  })

//  export
export default {
    globalUrlHttp,
    globalUrl,
    abc,
    initObj
}
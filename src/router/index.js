import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      redirect: '/listblog'
    },
    {
      path: '*',
      redirect: '/listblog'
    },
    {
      path: '/addblog',
      name: 'AddBlog',
      component: () => import('@/components/AddBlog') 
    },
    {
      path: '/listblog',
      name: 'ListBlog',
      component: () => import('@/components/ListBlog') 
    },
    {
      path: '/editblog/:id',
      name: 'EditBlog',
      component: () => import('@/components/EditBlog') 
    },
    {
      path: '/detailblog/:id',
      name: 'DetailBlog',
      component: () => import('@/components/DetailBlog') 
    }
  ]
})
